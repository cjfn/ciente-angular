import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-cmp',
   templateUrl: '../../views/new-cmp.component.html',
  styleUrls: ['./new-cmp.component.css']
})
export class NewCmpComponent implements OnInit {

  public title:' string;
  public description: string;

  constructor(){
  	this.title ='Componente 2';
  	this.description = 'componente 2 de angular'
  }

  ngOnInit() {
  }

}
